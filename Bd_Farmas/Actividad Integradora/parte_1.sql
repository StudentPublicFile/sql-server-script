--Para compras:

--1.	El total de compras realizadas
select sum(TOTAL) from tm_compras;

--2.	El total de compras realizadas por año
select YEAR(fechainicio), sum(total) from tm_compras group by YEAR(fechainicio);

--3.	El total de compras realizadas por año y mes
select YEAR(fechainicio), month(fechainicio), sum(total) from tm_compras group by YEAR(fechainicio), month(fechainicio);

--4.	El numero de Facturas de compras
select count(total) from tm_compras

--5.	El numero de Facturas de compras por Año
select YEAR(fechainicio), count(total) from tm_compras group by YEAR(fechainicio);

--6.   El numero de Facturas de compras por Año y mes
select YEAR(fechainicio), month(fechainicio), count(total) from tm_compras group by YEAR(fechainicio), month(fechainicio);

--7.   El total comprado por proveedor
select Fkproveedor, RazonSocial, sum(total)
from tm_compras C inner join tc_proveedor P on (c.FkProveedor = p.clave)
group by FkProveedor, RazonSocial
order by sum(total) desc

--8.   El total comprado por proveedor en cada Año
SELECT FkProveedor, RazonSocial,  YEAR(fechainicio) AS Año, SUM(Total) AS TotalAnual
FROM tm_compras C INNER JOIN tc_proveedor P ON (C.FkProveedor = P.clave)
GROUP BY FkProveedor, RazonSocial, YEAR(fechainicio)
ORDER BY SUM(Total) DESC;

--9.	El total comprado por proveedor en cada Año y mes
SELECT FkProveedor, RazonSocial, YEAR(FechaInicio) AS Año, MONTH(FechaInicio) AS Mes, SUM(Total) AS TotalMensual
FROM tm_compras C INNER JOIN tc_proveedor P ON (C.FkProveedor = P.clave)
GROUP BY FkProveedor, RazonSocial, YEAR(FechaInicio), MONTH(FechaInicio)
ORDER BY FkProveedor, RazonSocial, Año, Mes, SUM(Total) DESC;

--10.	El total comprado por tipo de proveedor
select g.Clave, p.RazonSocial, sum(total)
from tm_compras C inner join tc_proveedor P on (c.FkProveedor = p.Clave)
inner join tcg_proveedor G on (p.Grupo = g.Clave)
group by g.Clave, p.RazonSocial

--11.	El total comprado por tipo de proveedor por Año
select g.Clave, p.RazonSocial, YEAR(fechainicio) AS Año, SUM(total) AS TotalAnual
from tm_compras C inner join tc_proveedor P on (c.FkProveedor = p.Clave)
inner join tcg_proveedor G on (p.Grupo = g.Clave)
group by g.Clave, p.RazonSocial, YEAR(FechaInicio)
order by SUM(total) desc

--12.	El total comprado por tipo de proveedor por Año y mes
select g.Clave, p.RazonSocial, YEAR(fechainicio) AS Año, MONTH(FechaInicio) AS Mes, SUM(total) AS TotalMensual
from tm_compras C inner join tc_proveedor P on (c.FkProveedor = p.Clave)
inner join tcg_proveedor G on (p.Grupo = g.Clave)
group by g.Clave, p.RazonSocial, YEAR(FechaInicio), MONTH(FechaInicio)
order by g.Clave, p.RazonSocial, Año, Mes, SUM(total) desc

--13.	El numero de piezas compradas por articulo
SELECT A.clave, A.nombre, SUM(D.Cantidad) AS NumPiezas
FROM tm_compras C INNER JOIN tm_comprasdetalles D
ON C.FkAlmacen = D.FkAlmacen and C.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre
ORDER BY SUM(D.Cantidad) DESC

--14.	El numero de piezas compradas por articulo por Año
SELECT A.clave, A.nombre, YEAR(C.FechaInicio) AS Año, SUM(D.Cantidad) AS NumPiezas
FROM tm_compras C INNER JOIN tm_comprasdetalles D
ON C.FkAlmacen = D.FkAlmacen and C.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre, YEAR(C.FechaInicio)
ORDER BY A.Clave, YEAR(C.FechaInicio)

--15.	El numero de piezas compradas por articulo por Año y mes
SELECT A.clave, A.nombre, YEAR(FechaInicio) AS Año, MONTH(FechaInicio) AS Mes, SUM(D.Cantidad) AS NumPiezas
FROM tm_compras C INNER JOIN tm_comprasdetalles D
ON C.FkAlmacen = D.FkAlmacen and C.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre, YEAR(C.FechaInicio), MONTH(FechaInicio)
ORDER BY A.Clave, YEAR(C.FechaInicio), MONTH(FechaInicio)

--16.	El total comprado por articulo
select FkClave, Nombre, sum(Cantidad)
from tm_comprasdetalles D inner join tc_articulos A on (D.FkClave = A.Clave)
group by FkClave, Nombre
order by sum(Cantidad) desc

--17.	El total comprado por articulo por Año
SELECT FkClave, Nombre,  YEAR(Cantidad) AS Año, SUM(Cantidad) AS TotalAnual
from tm_comprasdetalles D inner join tc_articulos A on (D.FkClave = A.Clave)
GROUP BY FkClave, Nombre, YEAR(Cantidad)
ORDER BY SUM(Cantidad) DESC;

--18.	El total comprado por articulo por Año y mes
SELECT FkClave, Nombre,  YEAR(Cantidad) AS Año, MONTH(Cantidad) AS Mes, SUM(Cantidad) AS TotalMensual
from tm_comprasdetalles D inner join tc_articulos A on (D.FkClave = A.Clave)
GROUP BY FkClave, Nombre, YEAR(Cantidad), MONTH(Cantidad)
ORDER BY FkClave, Nombre, Año, Mes, SUM(Cantidad) DESC;

--19.	El total comprado por tipo de articulo
select W.Clave, W.Formula, SUM(X.Total) AS Total
from tc_articulos W inner join tm_comprasdetalles X on (W.Clave = X.FkClave)
group by W.Clave, W.Formula
order by SUM(X.Total) desc

--20.	El total comprado por tipo de articulo por Año
select W.Clave, W.Formula, YEAR(X.Total) AS Año, SUM(X.Total) AS TotalAnual
from tc_articulos W inner join tm_comprasdetalles X on (W.Clave = X.FkClave)
group by W.Clave, W.Formula, YEAR(X.Total)
order by SUM(X.Total) desc

--21.	El total comprado por tipo de articulo por Año y mes
select W.Clave, W.Formula, YEAR(X.Total) AS Año, MONTH(X.Total) AS Mes, SUM(X.Total) AS TotalMensual
from tc_articulos W inner join tm_comprasdetalles X on (W.Clave = X.FkClave)
group by W.Clave, W.Formula, YEAR(X.Total), MONTH(X.Total)
order by SUM(X.Total) desc

--22.	El top 10 de los art�culos m�s comprados
SELECT TOP 10 FkClave, Nombre, SUM(Cantidad) AS TotalComprado
FROM tm_comprasdetalles D INNER JOIN tc_articulos A ON (D.FkClave = A.clave)
GROUP BY FkClave, Nombre
ORDER BY SUM(Cantidad) DESC

--23.	El top 10 de compras a proveedor
SELECT TOP 10 Fkproveedor, RazonSocial, SUM(total)
FROM tm_compras C INNER JOIN tc_proveedor P ON (c.FkProveedor = p.clave)
GROUP BY FkProveedor, RazonSocial
ORDER BY SUM(total) DESC
