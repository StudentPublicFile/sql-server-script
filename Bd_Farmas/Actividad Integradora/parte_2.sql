--Para ventas:

--24.	El total de ventas realizadas
SELECT SUM(total) AS Total_Ventas FROM tm_remision

--25.	El total de ventas realizadas por año
SELECT YEAR(Fecha) AS Año, SUM(Total) AS Total_Ventas FROM tm_remision
GROUP BY YEAR(Fecha)

--26.	El total de ventas realizadas por año y mes
SELECT YEAR(Fecha) AS Año, MONTH(Fecha) AS MES, SUM(Total) AS Total_Ventas FROM tm_remision
GROUP BY YEAR(Fecha), MONTH(Fecha)
ORDER BY YEAR(Fecha), MONTH(Fecha)

--27.	El numero de Facturas de ventas
SELECT COUNT(Total) AS Num_Facturas FROM tm_remision

--28.	El numero de Facturas de ventas por año
SELECT YEAR(Fecha) AS Año, COUNT(Total) AS Num_Facturas FROM tm_remision
GROUP BY YEAR(Fecha)

--29.	El numero de Facturas de ventas por año y mes
SELECT YEAR(Fecha) AS Año, MONTH(Fecha) AS MES, COUNT(Total) as Num_Facturas FROM tm_remision
GROUP BY YEAR(Fecha), MONTH(Fecha)
ORDER BY YEAR(Fecha), MONTH(Fecha)

--30.	El total vendido por cliente
SELECT idcliente, RazonSocial, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY R.IdCliente, C.RazonSocial
ORDER BY SUM(Total) DESC

--31.	El total vendido por cliente en cada año
SELECT YEAR(R.Fecha) AS Año, idcliente, RazonSocial, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY YEAR(R.Fecha), R.IdCliente, C.RazonSocial
ORDER BY SUM(Total) DESC

--32.	El total vendido por cliente en cada año y mes
SELECT YEAR(R.Fecha) AS Año, MONTH(Fecha) AS MES, idcliente, RazonSocial, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY YEAR(R.Fecha), MONTH(Fecha), R.IdCliente, C.RazonSocial
ORDER BY SUM(Total) DESC

--33.	El total vendido por tipo de cliente
SELECT idcliente, RazonSocial, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY IdCliente, RazonSocial
ORDER BY IdCliente, RazonSocial

--34.	El total vendido por tipo de cliente por año
SELECT idcliente, RazonSocial, YEAR(fecha) AS Año, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY IdCliente, RazonSocial, YEAR(fecha)
ORDER BY IdCliente, RazonSocial, YEAR(fecha)

--35.	El total vendido por tipo de cliente por año y mes
SELECT idcliente, RazonSocial, YEAR(fecha) AS Año, MONTH(fecha) AS Mes, SUM(total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY IdCliente, RazonSocial, YEAR(fecha), MONTH(fecha)
ORDER BY IdCliente, RazonSocial, YEAR(fecha), MONTH(fecha)

--36.	El total vendido por agente de ventas
SELECT IdVendedor, Nombre, SUM(total)
FROM tm_remision R INNER JOIN tc_vendedor V ON R.IdVendedor = V.Clave
GROUP BY IdVendedor, Nombre
ORDER BY IdVendedor, Nombre

--37.	El total vendido por agente de ventas en cada año
SELECT IdVendedor, Nombre, YEAR(fecha) AS Año, SUM(total)
FROM tm_remision R INNER JOIN tc_vendedor V ON R.IdVendedor = V.Clave
GROUP BY IdVendedor, Nombre, YEAR(fecha)
ORDER BY IdVendedor, Nombre, YEAR(fecha)

--38.	El total vendido por agente de ventas en cada año y mes
SELECT IdVendedor, Nombre, YEAR(Fecha) AS Año, MONTH(Fecha) AS Mes, SUM(total)
FROM tm_remision R INNER JOIN tc_vendedor V ON R.IdVendedor = V.Clave
GROUP BY IdVendedor, Nombre, YEAR(fecha), MONTH(fecha)
ORDER BY IdVendedor, Nombre, YEAR(fecha), MONTH(fecha)

--39.	El total vendido por ruta
SELECT R.IdRuta, T.Nombre, T.Localidades, SUM(R.Total)
FROM tm_remision R INNER JOIN tcg_rutas T ON R.IdRuta = T.clave
GROUP BY R.IdRuta, T.Nombre, T.Localidades
ORDER BY SUM(R.Total) DESC

--40.	El total vendido por ruta en cada año
SELECT R.IdRuta, T.Nombre, T.Localidades, YEAR(R.Fecha) AS Año, SUM(R.Total)
FROM tm_remision R INNER JOIN tcg_rutas T ON R.IdRuta = T.clave
GROUP BY YEAR(R.Fecha), R.IdRuta, T.Nombre, T.Localidades
ORDER BY R.IdRuta, T.Nombre, T.Localidades

--41.	El total vendido por ruta en cada año y mes
SELECT R.IdRuta, T.Nombre, T.Localidades, YEAR(R.Fecha) AS Año, MONTH(R.Fecha) AS MES, SUM(R.Total)
FROM tm_remision R INNER JOIN tcg_rutas T ON R.IdRuta = T.clave
GROUP BY YEAR(R.Fecha), MONTH(R.Fecha), R.IdRuta, T.Nombre, T.Localidades
ORDER BY SUM(R.Total) DESC

--42.	El total vendido por articulo (remisi�n detalles)
SELECT A.clave, A.nombre, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre
ORDER BY SUM(D.Total) DESC

--43.	El total vendido por articulo por año
SELECT A.clave, A.nombre, YEAR(R.Fecha) AS Año, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre, YEAR(R.Fecha)
ORDER BY A.Clave, YEAR(R.Fecha)

--44.	El total vendido por articulo por año  y mes
SELECT A.clave, A.nombre, YEAR(R.Fecha) AS Año, MONTH(Fecha) AS Mes, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre, YEAR(R.Fecha), MONTH(Fecha)
ORDER BY A.Clave, YEAR(R.Fecha), MONTH(Fecha)

--45.	El total vendido por tipo de articulo
SELECT A.clave, A.Formula, SUM(D.total)
FROM tm_remisiondetalles D INNER JOIN tc_articulos A
ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Formula
ORDER BY A.Clave, A.Formula

--46.	El total vendido por tipo de articulo por año
SELECT A.clave, A.Formula, YEAR(R.Fecha) AS Año, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Formula, YEAR(R.Fecha)
ORDER BY A.Clave, YEAR(R.Fecha)

--47.	El total vendido por tipo de articulo por año y mes
SELECT A.clave, A.Formula, YEAR(R.Fecha) AS Año, MONTH(Fecha) AS MES, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Formula, YEAR(R.Fecha), MONTH(Fecha)
ORDER BY A.Clave, YEAR(R.Fecha), MONTH(Fecha)

--48.	El top 10 de los art�culos m�s vendidos
SELECT TOP 10 A.clave, A.nombre, SUM(D.total)
FROM tm_remision R INNER JOIN tm_remisiondetalles D
ON R.FkAlmacen = D.FkAlmacen and R.Folio = D.Folio
INNER JOIN tc_articulos A ON D.FkClave = A.Clave
GROUP BY A.Clave, A.Nombre
ORDER BY SUM(D.Total) DESC

--49.	El top 10 de los mejores clientes
SELECT TOP 10 idcliente, RazonSocial, SUM(Total)
FROM tm_remision R INNER JOIN tc_cliente C ON R.IdCliente = C.Clave
GROUP BY IdCliente, RazonSocial
ORDER BY SUM(total) DESC

--50.	El top 10 de los mejores agentes de ventas
SELECT TOP 10 IdVendedor, Nombre, SUM(total)
FROM tm_remision R INNER JOIN tc_vendedor V ON R.IdVendedor = V.Clave
GROUP BY IdVendedor, Nombre
ORDER BY IdVendedor, Nombre